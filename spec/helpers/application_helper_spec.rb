require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    subject { full_title(page_title) }

    context 'page_titleがHelpの場合' do
      let(:page_title) { 'Help' }

      it 'Help | BASE_TITLEが表示される' do
        is_expected.to eq 'Help | BIGBAG Store'
      end
    end

    context 'page_titleが空の場合' do
      let(:page_title) { '' }

      it 'BASE_TITLEが表示される' do
        is_expected.to eq 'BIGBAG Store'
      end
    end

    context 'page_titleがnilの場合' do
      let(:page_title) { nil }

      it 'BASE_TITLEが表示される' do
        is_expected.to eq 'BIGBAG Store'
      end
    end
  end
end
