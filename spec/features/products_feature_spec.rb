require 'rails_helper'

RSpec.feature 'Products_features', type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario '商品詳細ページで表示されているコンテンツ' do
    expect(page).to have_current_path potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} | BIGBAG Store"
    expect(page).to have_link 'Home', href: potepan_index_path
    expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(taxon.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end

  scenario '関連商品の商品名と商品価格が表示されている' do
    related_products.each do |related_product|
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
  end

  scenario '関連商品名をクリックした時、クリックした商品の詳細ページに移動する' do
    related_products.each do |related_product|
      click_on related_product.name
      expect(page).to have_current_path potepan_product_path(related_product.id)
    end
  end

  context '関連商品が4個より多く生成された場合' do
    let(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    scenario '関連商品を4個表示する' do
      expect(page).to have_selector '.productBox', count: 4
    end
  end
end
