require 'rails_helper'

RSpec.feature 'Categories_features', type: :feature do
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:taxonomy) { create(:taxonomy, name: 'Categories') }
  let(:taxon) { create(:taxon) }
  let(:other_taxonomy) { create(:taxonomy) }
  let(:other_taxon) { create(:taxon, parent: other_taxonomy.root) }
  let(:other_product) { create(:product, taxons: [other_taxon]) }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario 'カテゴリーページで表示されているコンテンツ' do
    expect(page).to have_current_path potepan_category_path(taxon.id)
    expect(page).to have_title "#{taxon.name} | BIGBAG Store"
    expect(page).to have_link 'Home', href: potepan_index_path
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
    expect(page).to have_content taxon.products.count
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  scenario '商品カテゴリー覧の商品分類をクリックした時、クリックしたカテゴリーページへ移動すること' do
    click_on taxon.name
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario '商品の分類(カテゴリー)ページで、関連しない商品が表示されないこと' do
    expect(page).to_not have_content other_product.name
  end

  scenario '商品名をクリックした時、クリックした商品の詳細ページへ移動すること' do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
