require 'rails_helper'

RSpec.describe 'Potepan::Categories', type: :request do
  describe 'GET #show' do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxonomy) { create(:taxonomy, name: 'Categories') }
    let(:taxon) { create(:taxon) }

    before do
      get potepan_category_path(taxon.id)
    end

    it 'レスポンスが正常であること' do
      expect(response).to be_successful
    end

    it '200レスポンスが返ってくること' do
      expect(response).to have_http_status '200'
    end

    it '商品名が含まれていること' do
      expect(response.body).to include product.name
    end

    it '商品価格が含まれていること' do
      expect(response.body).to include product.display_price.to_s
    end

    it 'カテゴリー名(大項目)が含まれていること' do
      expect(response.body).to include taxonomy.name
    end

    it '商品分類名(中項目)が含まれていること' do
      expect(response.body).to include taxon.name
    end

    it '商品の数が含まれていること' do
      expect(response.body).to include taxon.products.count.to_s
    end
  end
end
