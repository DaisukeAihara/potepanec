require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :request do
  describe 'GET #show' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it 'レスポンスが正常であること' do
      expect(response).to be_successful
    end

    it '200レスポンスが返ってくること' do
      expect(response).to have_http_status '200'
    end

    it '商品名が含まれていること' do
      expect(response.body).to include product.name
    end

    it '商品説明が含まれていること' do
      expect(response.body).to include product.description
    end

    it '商品価格が含まれていること' do
      expect(response.body).to include product.display_price.to_s
    end

    it '関連商品が4つ表示されていること' do
      expect(controller.instance_variable_get('@related_products').count).to eq 4
    end
  end
end
