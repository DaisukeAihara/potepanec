require 'rails_helper'

RSpec.describe 'Potepan::ProductDecorator', type: :model do
  describe 'related_products_test' do
    subject { product.related_products }

    let(:taxonomy) { create(:taxonomy) }
    let(:taxon1) { create(:taxon, taxonomy: taxonomy) }
    let(:taxon2) { create(:taxon, taxonomy: taxonomy) }
    let(:taxon3) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon1, taxon2, taxon3]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon1, taxon2, taxon3]) }
    let(:other_taxon) { create(:taxon) }
    let!(:unrelated_product) { create(:product, taxons: [other_taxon]) }

    it '関連商品が正常に表示されていること' do
      is_expected.to eq related_products
    end

    it '関連しない商品が含まれていないこと' do
      is_expected.to_not include unrelated_product
    end

    it '選択中の商品の関連商品には、商品自身が含まれないこと' do
      is_expected.to_not include product
    end

    it '関連商品の重複が無いこと' do
      is_expected.to eq subject.distinct
    end
  end
end
